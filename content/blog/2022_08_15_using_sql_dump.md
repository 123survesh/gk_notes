---
title: Using SQL Dump
date: 2022-08-15
tags: [sql-dump, docker, postgres]
draft: false
---

### Context
For GNUKhata we have hosted a demo instance in a digitalocean droplet and the community has been using it for the past few months. I thought it would be a good source of data for testing DB migrations after adding new changes. So I am noting down the steps involved in taking a sql dump and then restoring it for usage.

### Good to know points
1. This method that I have used here WILL LOSE ALL THE EXISTING DATA, so to be only used when existing data is not required.
2. The dump used here is of type .sql, i.e. text based, hence pg_restore cannot be used. If the restore is done with pg_restore, the recreate DB step can be added with few extra parameters to the function call.

### SQL dump usage notes

1. Take a SQL dump from the instance that is packaged with docker,
```
docker exec -i build_db_1 /bin/bash -c "PGPASSWORD=gkadmin pg_dump --username gkadmin gkdata" > gkdump.sql
```

2. Copy dump from remote to local,
```
scp user@ip:/remote/path/to/gkdump.sql /local/path/to/gkdump.sql
```

3. Recreate the DB gkdata
```
drop database gkdata;
create database gkdata;
```

4. Import data from our dump to DB
```
sudo psql -U postgres -d gkdata < gkdump.sql
```