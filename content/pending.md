---
title: pending
date: pending
tags: []
draft: false
---

|BUGS|
======
- [ ] Deleting a Del Note is causing the delnote counter to go back to the deleted del notes number
- [ ] Allowing the use of no Godown
- [ ] Sale of negative stock is possible
- [ ] Billwise adjust page
- [ ] Balance Sheet page
- [ ] Stock data is changed after changing contacts in invoices  (Not able to reproduce)
- [ ] i am being informed by the intern that after every few invoices, the software needs hard refresh because the software shows wrong gstin and incorrect details
- [ ] [GSTR3B] The JSON has issue in section 3.2
- [ ] [GSTR1] Validate excel with offline tool
- [ ] Issue with vouchers created for VAT
- [ ] https://gitlab.com/gnukhata/gkapp/-/issues/205  
- [ ] After resetting username in login flow, fin years are not populated in drop down

|NOT REPRODUCED BUGS|
=====================
- [ ] [NOT_REPRODUCED] [BUG] Auto population of place of supply is not working
- [ ] [NOT_REPRODUCED] [BUG] Vouchers are not deleted when invoices are cancelled
  Note: 
  1. The above bug was not reproduceable. Vouchers are cancelled when invoices are cancelled. But Vouchers are not linked in the cancelled invoice profile
  2. R2: i found that profit and loss was different from view register. i found in ledger had entries (vouchers) that were not in sales register.

|PENDING|
=========

## High Priority
- [ ] Add DelNote Id counter configuration
- [ ] [BIG] invoice level discounting
  https://www.patriotsoftware.com/accounting/training/help/adding-discounts-to-your-customer-invoice/
  https://niharikatechnologiestallysoftware.in/discount-invoices-in-tally/
  https://help.tallysolutions.com/article/Tally.ERP9/Tax_India/gst/gst_discount.htm
  https://www.zoho.com/in/books/kb/invoices/invoice-discounts.html
  https://help.zoho.com/portal/en/community/topic/discount-amount-on-invoice
  Discount for the whole invoice (inclusive of tax, exclusive of tax)
- [ ] [small] show tax rate/HSN in transaction item dropdown (near out of stock)

## Medium Priority
- [ ] Return rate flag without tooltip in create invoice page
- [ ] Port GSTR-3B excel to government excel format

## Low Priority
- [x] add loaders for invoice edit data fetch
- [ ] After cancelling an invoice it must refresh to reflect its cancelled state in its profile page 
- [ ] [#B] Remember the invoice type, sale/purchase
- [ ] [#B] Upload attachments in transaction forms [Invoice edit, Del Note, PS Order]
- [ ] Receipt Voucher for credit invoice repayment flow
- [ ] Check and update a way to apply the global date settings throughout the app
- [ ] Same for decimal in rates. We are not able to input 3 decimal places in rate.
- [ ] when creating a new purchase, after save ,it resets to sales. And add new supplier resets to customer.
- [ ] Make the print screen visibility config
- [ ] Name the triplicate by their names instead of duplicate and triplicate
- [ ] Add a setting that switches between using godowns and delivery notes
- [ ] Add Del Note Triplicate 
- [ ] [#C] Improve GSTIN validation : 
    Also, I have an idea how to improve the gstin verification procedure.
    If we login to gst, we can call multiple gstins without having to add captcha every time.
    So, login to gst, enter login captcha and then call the search api.
    If you want, I can give you a live gst credentials to test this
    https://www.youtube.com/watch?v=TPkdsFAtdcQ
- [ ] [#C] Bank Reconciliation use check boxes instead of radio buttons
- [ ] [#C] [PENDING] Voucher (Lock, clone)
- [ ] [#C] When there are no invoices to edit, add a message where the invoice selector is to notify about it
- [ ] [#C] Add a confirmation box forcing people to use the state and pan same as GSTIN
- [ ] [#C] [BUG] Remove services from the product/service list in create transfer note form
- [ ] [#A] HTML to pdf, page splitting, table splitting
- [ ] [#A] Check if its possible to fetch seller or purchaser state from the invoice stored. Because CGST/SGST/IGST is based on the state of the person at the time the invoice was created.
- [ ] [#B] Windows installer updater
- [ ] [#B] Release Notes for 7.1
- [ ] [#C] Add ledger links to budget report
- [ ] [#B] [BUG] When coming from another page, autocomoplete is not retaining the data.
- [ ] [#B] Make the Balance sheet tables equal in height when printing
- [ ] [#B] [BUG] When coming from Invoice Profile, Bill Adjustment page is not auto loading, but does on page refresh
- [ ] [#C] (Balance sheet) Show only groups, subgroups, accounts buttons
- [ ] [#B] In Filters, Need ability to search for different types of fields available
   e.g. Invoice No., Date, UserName
- [ ] [#C] Add ability to trigger loading animation in transaction-details components
- [ ] [#C] Add Loading animation in places missed
Test the various serach libraries
- [ ] [Global Config] Choose between GST, NO GST, VAT (Ask about no gst modes for per item and per transaction)
- [ ] UI for config with checkboxes

## Unrated Tasks
- [ ] Add bulk customer/products adding feature and export
- [ ] Change the invoice edit flow
- [ ] Implement UQC in gstr1 + add a warning if wrong uqc is added
- [ ] Replace autocomplete with v-complete in all places
- [ ] Invoice no unique constraint must be for sales only not purchases
- [ ] Invoice table extra rates
      https://gitlab.com/gnukhata/gkcore/-/issues/479
      Existing PRs
      https://gitlab.com/gnukhata/gkcore/-/merge_requests/575
      https://gitlab.com/gnukhata/gkwebapp/-/merge_requests/1178
- [ ] Invoice table, entering total must calculate price feature
- [ ] [GSTR3B] Add support for non GST inward (5), composition and UIN (3.2)
- [ ] Add Easy to use UI for payment/ reciept voucher
- [ ] Add Inventory value (WIP)
- [ ] Add auto narration for all transaction forms
- [ ] Tax Inclusive / Exclusive (Bill Table)
- [] Add edit/delete user flow
- [] edit user invite

### Rectification Requests
1. there was a sales made during the 0 tax rate.
now that the item  is at corect tax rate, the entry now has wrong tax 0. is there a way we can detect these type of entries and a way to inform users about it. also, to correct them
- Correct Invoices, with proper tax rates of items
2. i am saying
for old entries made before we started mandating primary godown, can you set a script that changes their "no godown" to "primary godown" that is company default
that way we have only 1 table going forward
like if you run the script in this company, the two sales would go from current table to primary godown because that is default.
if there is a godown set already in an item or transaction, leave that but for null godown or no godown, set that to primary godown based on company default setting
- Migrate every stock from the no godown table to the primary godown table


|GST Compliance|
================
- [x] GSTIN proper format. current implementation of putting state first, then pan number then rest of digits is wrong. instead allow users to paste GSTIN in one go and then infer whatever you want from there.
- [x] GSTIN verify via regex and formula
- [x] place of supply rules for tax class selection/ point of taxation
- [x] more gst tax rates
- [x] invoice triplicate setting
- [x] [MEDIUM] purchase invoice to have "supplier invoice number/date"
- [x] [BIG] set applicability date to GST rates (from and to)
- [x] [BIG] add option for registration type to contacts. ; regular, unregistered, consumer, composition
- [x] [BIG] allow contact to be set to other terrritory/transporter/ UIN, embassy/sez or e-commerce operator
- [x]  reason for GST debit/credit notes
- [x] [MEDIUM] description in HSN

- [ ] reporting UQC in units of measurement gkwebapp#1246
- [ ] gst summary of all returns to have full hyperlinking to invoices and necessary summary totals
- [ ] essential GST returns in JSON (or excel format) for GSTR-1 and GSTR-3B.
- [ ] option to select periodicity for GST returns. monthly or quarterly GSTR-1 including IFF (same for GSTR-3B)

- [ ] [BIG] add gst applicability. including "gst applicable from". (if no gst is added, this option is off by default, no gst is used in the software.)
- [ ] [SMALL] show tax rate/HSN in transaction item dropdown (near out of stock)
- [ ] allow users to define "inclusive of tax" or "exclusive of tax" gkwebapp#1157
- [ ] [MEDIUM] invoice level discounting
- [ ] [BIG] TAX on invoice shipping charges (landed cost type)
- [ ] cess. both fixed rate and advalorum #189
- [ ] HSN/SAC wise tax summary to invoice templates

- [ ] Implement IFF and QRMP (Quaterly Returns Monthly Payment)
    https://cleartax.in/s/invoice-furnishing-facility-iff/
    https://gsthero.com/invoice-furnishing-facility-iff-for-quarterly-filers/
    https://cleartax.in/s/quarterly-return-monthly-payment-qrmp-scheme-gst
    https://www.zoho.com/in/books/gst/qrmp-scheme.html
    https://www.gstzen.in/a/quarterly-return-filing-and-monthly-payment-of-taxes-qrmp.html
    https://img-www.gstzen.in/wp-content/uploads/2021/03/18115751/QRMP-3.png