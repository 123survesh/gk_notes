---
title: 2022-04-27
date: 2022-04-27
tags: []
draft: false
---

### Bugs
 [GeneralBugs]
- [x] [3] Update the invoice number DB constraint
- [x] Vat is not handled in item creation and invoice page, total table
      dont keep vat as a state to vat details map in bill table, use only the value respective to the tax state

### Features
- [x] ADD UQC

### Chore
- [x] Right align the money and qty columns in the invoices list tables of gst returns