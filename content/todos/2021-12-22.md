---
title: 2021-12-22
date: 2021-12-22
tags: []
draft: false
---

- [x] Add default godown feature
- [x] [GST] Invoice Triplicate setting
- [x] Add automatic Delivery Notes to Invoices so that we can select the required godowns
  - [x] Create DEL Note automatically
  - [x] Use the godown chosen to get the stock qty
  - [x] Create an API for the qty stock, rather than hitting the reports API a lot
  - [x] Show godown name in invoice profile
    - [x] fetch the deliverynote details as well
- [x] Upload attachments in transaction forms
  - [x] show Preview of the files uploaded
  - [x] show the attachments in invoice profile page