---
title: Why this website exists?
date: 
tags: []
draft: false
---

<h1>{{ .Title }}</h1>

This website is a way for me to use the powers of git and version control the notes that I took while working on GNUKhata. 

What is [GNUKhata](https://gnukhata.gitlab.io/)? Its a double entry accounting software (Licensed AGPLv3).